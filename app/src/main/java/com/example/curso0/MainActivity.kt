package com.example.curso0

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)




        btnVerificar.setOnClickListener {

        tvResultado.text=   when (edtNacimiento.text.toString().toInt()){

                in 1994..2010->{

                    ivRasgo.setImageResource(R.mipmap.irreverencia)
            "Poblacion: 7800000"
            }
                in 1981..1993->{
                    ivRasgo.setImageResource(R.mipmap.frustracion)
            "Poblacion:  7200000"
                }
                in 1969..1980->{
                    ivRasgo.setImageResource(R.mipmap.exito)
            "Poblacion:  9300000"
                }
                in 1949..1968->{
                    ivRasgo.setImageResource(R.mipmap.ambicion)
            "Poblacion: 12200000"
                }

                in 1930..1948->{
                    ivRasgo.setImageResource(R.mipmap.austeridad)
            "Poblacion: 6300000"

                }
                else ->{
                    ivRasgo.setImageResource(R.drawable.ic_launcher_foreground)
            "Poblacion: Ninguna"
                }

            }





        }

    }
}
